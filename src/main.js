import scss from './stylesheets/app.scss';

import Vue from 'vue';
import Resource from 'vue-resource';
import Router from 'vue-router';

import App from './components/App.vue';
import Home from './components/Home.vue';
import Services from './components/Services.vue';
import Contact from './components/Contact.vue';

// Install plugins
Vue.use(Router);
Vue.use(Resource);

// route config
let routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
      path: '/services',
      name: 'services',
      component: Services
  },
  {
      path: '/contact',
      name: 'contact',
      component: Contact,
      params: {
          service: null
      }
  },
  { path: '*', redirect: '/home' }
];

// Set up a new router
let router = new Router({
    routes: routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});

Vue.http.options.root = '/root';
Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk';

// Start up our app
new Vue({
    router: router,
    http: {
        emulateJSON: true,
        emulateHTTP: true
    },
    render: h => h(App)
}).$mount('#app');